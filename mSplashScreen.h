#pragma once

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "mMenuScene.h"

class mSplashScreen : public cocos2d::CCLayer
{
public:

	static cocos2d::CCScene* createscene();
	virtual bool init();
	mSplashScreen(void);
	~mSplashScreen(void);

	void toMenuScene(float dt);

	CCLabelTTF* ttf1;
	CCLabelTTF* highscorelabel;
	CREATE_FUNC(mSplashScreen);
};
