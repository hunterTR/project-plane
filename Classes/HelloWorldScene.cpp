#include "HelloWorldScene.h"
#include "AppMacros.h"

USING_NS_CC;



HelloWorld::HelloWorld()
{
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	mWorld = new b2World(gravity);
	mWorld->SetAllowSleeping(true);
	mWorld->SetContinuousPhysics(true);
	mWorld->SetContactListener(&contactlistener);
	score = 0;
	mAP = new mAirPlane(500, 600);
	levelcreator = new mLevel(mWorld, this,&winsize);
	round = 1;
	
}


HelloWorld::~HelloWorld()
{

	delete mWorld;
	mWorld = NULL;
}

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();
	mIndependentLayer* deneme = mIndependentLayer::create();
	
    // add layer as a child to scene
    scene->addChild(layer,4);
	scene->addChild(deneme, 8);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
	this->setTouchEnabled(true);


	bool isSoundOn = CCUserDefault::sharedUserDefault()->getBoolForKey("sound", 1);

	if (isSoundOn)
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("bgv3.wav", true);
	CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(5);

	CCUserDefault::sharedUserDefault()->setIntegerForKey("gametype", 1);
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	
	winsize = CCDirector::sharedDirector()->getWinSize();
	this->setTag(1);
	sky= CCSprite::create("sky.png");
	sky->setAnchorPoint(ccp(0, 0));
	sky->setPosition(ccp(0, -768));
	this->addChild(sky);

	background = CCSprite::create("background.png");
	background->setAnchorPoint(ccp(0, 0));
	background->setPosition(ccp(0, -768));
	this->addChild(background);

	/*


	mParallax = extendedparallax::node();

	CCPoint bgspeed = ccp(0,1.5f);
	CCPoint fgspeed = ccp(0.0, 0.0);
	mParallax->addChild(background, 1,fgspeed, ccp(0,0));
	mParallax->addChild(deneme,0, bgspeed, ccp(0, 0));
	mParallax->setAnchorPoint(ccp(0, 0));
	//mParallax->addChild(ground,0,fgspeed,ccp(420,25));
	mParallax->setPosition(ccp(0,-768));

	this->addChild(mParallax);

	*/




	mAP->setWorldandcreatebody(mWorld);
	this->addChild(mAP->getSprite());


	
		
	levelcreator->createRandomLevel(1);
	
	sides = CCSprite::create("sides.png");
	sides->setAnchorPoint(ccp(0, 0));
	sides->setPosition(ccp(0, -768));
	this->addChild(sides,10);

	counter = CCLabelTTF::create(" ", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
	counter->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 50));
	counter->setColor(ccc3(255, 255, 255));
	this->addChild(counter,11);





	this->schedule(schedule_selector(HelloWorld::updateScreen));
    
    return true;
}




void HelloWorld::updateScreen(float dt)
{
	//printTime();

	//CCLOG("entering updatescreen in helloworld");
	int velocityIterations = 8;
	int positionIterations = 1;


	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	mWorld->Step(dt, velocityIterations, positionIterations);

	//CCLOG("entering iterating bodies");
	for (b2Body* b = mWorld->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL)
		{
			CCSprite* myActor = (CCSprite*)b->GetUserData();




			if (myActor->getTag() == 100)
			{
				//GAMEOVER BITCH!
		/*		CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("crash.wav");     //SOUND EFFECT
				CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("crash.wav");*/  
				
				highs = CCUserDefault::sharedUserDefault()->getIntegerForKey("highscore", 1);
				CCUserDefault::sharedUserDefault()->setIntegerForKey("actualscore", score);
				if (score > highs)
				{
					CCUserDefault::sharedUserDefault()->setIntegerForKey("highscore", score);
				}
				this->schedule(schedule_selector(HelloWorld::gameOverBro), 0.3f);
			}

			myActor->setPosition(CCPointMake(b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO));
			myActor->setRotation(-1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));


			if (mAP->getSprite()->getPosition().y < myActor->getPosition().y - myActor->getContentSize().height *myActor->getScaleY()-200)
			{
				deletevector.push_back(b);
			}
		}
	}

	for (int i = 0; i<deletevector.size(); i++)
	{
		if (deletevector[i] !=NULL)
		{
			CCSprite* anan = (CCSprite*)deletevector[i]->GetUserData();
			deletevector[i]->SetUserData(NULL);
			deletevector[i]->SetAwake(false);
			mWorld->DestroyBody(deletevector[i]);
			deletevector[i] = NULL;  // KEY MOMENT !! NEVER FORGET THIS SHIT AGAIN
			this->removeChild(anan, true);
		}
	
	}
	deletevector.clear();
	mAP->move();
	cameraMovement();

	CCPoint p = mAP->pSprite->getPosition();
	
	int a = mAP->pSprite->getPosition().y;
	
	// SCORE CALCULATING ALGORTIHM ( Every obstacle gonna have same distance from eachother ) 
	int tmp = score; // this is for increasing speed.
	score = (600-a) / 200;

	if (tmp != score)
		mAP->increaseSpeed();
	
		CCString* sscore = CCString::createWithFormat("Score: %d", score);  //delete vector size � �imdilik kullan�yom.
		counter->setString(sscore->getCString());
	
	

		// Add more level when reach the exact score
		if (score > 30 * round - 10)
		{
			round++;
			levelcreator->createRandomLevel(round);
			
		}




}


void HelloWorld::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
	CCTouch* mTouch = (CCTouch*)(touches->anyObject());

	CCPoint location = mTouch->getLocation();
	location = CCDirector::sharedDirector()->convertToGL(location);

	if (location.x > winsize.width / 2)
	{
		mAP->changedirection(1);
		
		//CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("change.wav");     //SOUND EFFECT
	}
	else
	{
		mAP->changedirection(0);
		//CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("change.wav");    //SOUND EFFECT
	} 




}

void HelloWorld::cameraMovement()
{
	//CAMERA MOVEMENT ALGORITHM
	float x, y, z;
	this->getCamera()->getEyeXYZ(&x, &y, &z);
	float temp = sky->getPositionY();



	y = mAP->pSprite->getPosition().y -600;
	sky->setPositionY(y);
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
	this->getCamera()->setCenterXYZ(x, y, 0);
	this->getCamera()->setEyeXYZ(x, y, z);

	if (background->getPosition().y > y)   // Background looping algorithm
	background->setPosition(ccp(x, y-768));
	if (sides->getPosition().y > y)   // Background looping algorithm
		sides->setPosition(ccp(x, y-768));

	counter->setPosition(ccp(100, y+720));

}

void HelloWorld::pausegame()
{
	do{

		ccColor3B c = { 100, 100, 100 };
		mPauseLayer *pauselayer = mPauseLayer::create();
		mLayerColor *denemeamk = mLayerColor::create();
		denemeamk->setTag(81);
		denemeamk->setColor(c);
		denemeamk->setOpacity(100);
		CC_BREAK_IF(!pauselayer);
		this->getParent()->addChild(denemeamk, 5);
		this->getParent()->addChild(pauselayer, 8);
	/*	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("menuclick.wav");      //SOUND EFFECT
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menuclick.wav");*/

	} while (0);
	this->onExit();


}


void HelloWorld::gameOverBro(float dt)
{
	do{
		ccColor4B c = { 100, 100, 0, 100 };
		mGameOverLayer *gameover = mGameOverLayer::create();
		CC_BREAK_IF(!gameover);
		mLayerColor *denemeamk = mLayerColor::create();
		denemeamk->setTag(81);
		denemeamk->setOpacity(100);
		this->getParent()->addChild(denemeamk, 5);
		this->getParent()->addChild(gameover, 8);

	} while (0);
	this->getParent()->getChildByTag(2)->onExit();
	this->onExit();
}