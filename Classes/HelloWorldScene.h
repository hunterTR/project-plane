#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "mAirPlane.h"
#include "mIndependentLayer.h"
#include "mLevel.h"
#include "mPauseLayer.h"
#include "mLayerColor.h"
#include "mGameOverLayer.h"
#include "extendedparallax.h"
#include "mContactListener.h"
//  objeleri silmede hala problem olabilir ona dikkat ! ama �uanl�k iyi g�r�n�yor
//  u�a��n hala  h�z ayarlamas�na ihtiyac� var.
//  men�ler-sceneler halledilecek.
// round olay� de�i�ildi her level 30 obstacle dan olu�uyor. ( not gereksiz yere fazladan body ler ��k�yor gibiydi ama �uanl�k �yle bi�ey yok gibi)
// menu transition olay�lar� yap�lcak. game over oldu�unda highscore g�sterilecek.  SPLASH screen yap�lacak.
// Stage ler i�in yeni bi layer yapmam laz�m.


class HelloWorld : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

	HelloWorld();
	~HelloWorld();
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
	void updateScreen(float dt);
	void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
	void cameraMovement();
	void addGround(const char* filename, int x, int y, int w, int h);
	void pausegame();
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
	void gameOverBro(float dt);
    // implement the "static node()" method manually
    CREATE_FUNC(HelloWorld);


private:
	b2World* mWorld;
	mAirPlane* mAP;
	CCSize winsize;
	CCLabelTTF* counter;
	CCLabelTTF* highscore;
	mLevel* levelcreator;
	CCSprite* background;
	CCSprite* sides;
	CCSprite* sky;
	int score;
	int round;
	int highs;
	vector<b2Body*> deletevector;
	CCParallaxNode* _backgroundNode;
	extendedparallax* mParallax;
	mContactListener contactlistener;
};

#endif // __HELLOWORLD_SCENE_H__
