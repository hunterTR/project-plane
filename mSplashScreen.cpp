#include "mSplashScreen.h"

CCScene* mSplashScreen::createscene()
{

	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();

		CC_BREAK_IF(!scene);

		// 'layer' is an autorelease object
		mSplashScreen *layer = mSplashScreen::create();
		CC_BREAK_IF(!layer);

		// add layer as a child to scene
		scene->addChild(layer);

	} while (0);

	// return the scene
	return scene;
}

bool mSplashScreen::init()
{
	bool bRet = false;

	do
	{

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());

		this->setTouchEnabled(true);


		CCSprite* splash = CCSprite::create("splash.png");
		splash->setAnchorPoint(ccp(0, 0));
		splash->setPosition(ccp(0, 0));
		this->addChild(splash);

		this->schedule(schedule_selector(mSplashScreen::toMenuScene));

		bRet = true;
	} while (0);

	return bRet;
}


void mSplashScreen::toMenuScene(float dt)
{

	CCScene * scene = NULL;
	do
	{

		scene = mMenuScene::createscene();

		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.5, scene));
	} while (0);
	this->removeFromParentAndCleanup(true);

}