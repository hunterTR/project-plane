#pragma once
#include "mContactListener.h"
#include "mAirPlane.h"


class mContactListener : public b2ContactListener
{
public:
	mContactListener(void);
	~mContactListener(void);

	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
};

