#include "mLevel.h"


mLevel::mLevel(b2World* world, CCLayer* layer,CCSize* winsiz)
{
	mWorld = world;
	mLayer = layer;
	srand(time(NULL));
	randomlengthofobstacle = 2;
	randomforlittlepassage = 10;
	randomtypeofobstacle = 3;
	winsize = winsiz;
	passagelong = 700;
	
}


mLevel::~mLevel()
{

}



void mLevel::createRandomLevel(float round)
{
	float h =80;
	leftsiders = 284;
	rightsiders = winsize->width - 284;
	if (round == 1)
	{
		lastposition = 400;
	}

	if (round <= 5)
	{
	h = 20 + round * 10;
	}
	else
	{
		h = 80;
	}
	
	

	randomforlittlepassage - 1;
	int y = lastposition;
	addGround(leftsiders - 20, y - passagelong - 200 * 30, 20, 200 * 30 +passagelong+400);
	addGround(rightsiders, y -passagelong - 200 * 30, 20, 200 *30 + passagelong+400);
	for (int i = 0; i < 30; i++)
	{
		
	//	addGround(leftsiders - 20, y -200, 20, 200);
		//addGround(rightsiders, y -200, 20, 200);
		int a = rand()%randomforlittlepassage;
		int b = rand() %randomtypeofobstacle;
		int c = rand() % randomlengthofobstacle;

		if (a == 2)
		{
			if (b == 0)
				addLittlePassage(0, y, h);
			else if (b==1)
				addLittlePassage(1, y, h);
			else if (b==2)
				addLittlePassage(2, y, h);
			//createlittlepassage slightly left

			//createlittlepassage slightly right

			//createlittlepassage slightly middle

		}
		else
		{
			if (i % 2 == 0)
			{// add obstacle to left
				if (c == 0)
				{
					addBigObstacle(0, y, h);
				}
				else
				{
					addSmallObstacle(0, y, h);

				}
			}
			else
			{//add obstacle to right
				if (c == 0)
				{
					addBigObstacle(1, y, h);
				}
				else
				{
					addSmallObstacle(1, y, h);

				}
			}
			
		
		}


		y -= 200;
		
	}

	// ADD LONG PASSAGE
	addLongPassage(y-passagelong);

	lastposition = y - passagelong -200;
		


}


void mLevel::addLittlePassage(int place ,int y,int h)
{
	int x, w;
	if (place == 0)  //middle
	{ 
		x = leftsiders;
		w = 180;
		addGround(x, y, w, h);
		
		x = rightsiders - w;
		addGround(x, y, w, h);





	}
	else if (place ==1)  // slightly right
	{
		x = leftsiders;
		w = 230;
		addGround(x, y, w, h);
	
		w = 130;
		x = rightsiders - w;
		addGround(x, y, w, h);
	
	}
	else if (place ==2 )
	{
		x = leftsiders;
		w = 130;
		addGround(x, y, w, h);

		w = 230;
		x = rightsiders - w;
		addGround(x, y, w, h);
	
	
	}

}
void mLevel::addSmallObstacle(int place, int y, int h)
{
	if (place == 0)
	{

		int x, w;
		w = 195;
		x = leftsiders;
		addGround(x, y, w, h);


	}
	else
	{

		int x, w;
		w = 195;
		x = rightsiders - w;
		addGround(x, y, w, h);
		
	}

}
void mLevel::addBigObstacle(int place,int y, int h)
{
	if (place == 0)
	{
	
	int x, w;
	w = 285;
	x = leftsiders;
	addGround(x, y, w, h);

	}
else
{

	int x, w;
	w = 285;
	x = rightsiders - w;
	addGround(x, y, w, h);

}
}

void mLevel::addLongPassage(int y)
{
	int x, w,h;
	w = 155;
	x = leftsiders;
	h = passagelong;


	addGround(x, y, w, h);

	w = 155;
	x = rightsiders - w;

	addGround(x, y, w, h);

}


void mLevel::addGround(int x, int y, int w, int h)
{
	CCSprite* mBox = CCSprite::create("table.png", CCRectMake(0, 0, w, h));
	mBox->setPosition(ccp(x, y));
	mBox->setTag(1);
	mLayer->addChild(mBox, 1);

	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(((float)x + (float)w / 2.0f) / PTM_RATIO, ((float)y + (float)h / 2) / PTM_RATIO);
	bodyDef.userData = mBox;
	b2Body* body = mWorld->CreateBody(&bodyDef);

	b2FixtureDef fixturedef;
	b2PolygonShape mShape;
	mShape.SetAsBox(float(w) / 2.0f / PTM_RATIO, float(h) / 2.0f / PTM_RATIO);
	fixturedef.shape = &mShape;
	fixturedef.density = 1.0f;
	fixturedef.friction = 0.3f;
	fixturedef.filter.categoryBits = C_OBSTACLE;
	fixturedef.filter.maskBits = C_AIRPLANE;
	body->CreateFixture(&fixturedef);

}


void mLevel::createwalls(int x, int y , int w, int h,int a)
{
	if (a == 1)
	{
		CCSprite* mBox = CCSprite::create("HelloWorld.png", CCRectMake(0, 0, w, h));
		mBox->setPosition(ccp(x, y));
		mBox->setTag(1);
		mLayer->addChild(mBox, 1);

		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		bodyDef.position.Set(((float)x + (float)w / 2.0f) / PTM_RATIO, ((float)y + (float)h / 2) / PTM_RATIO);
		bodyDef.userData = mBox;
		wall1 = mWorld->CreateBody(&bodyDef);

		b2FixtureDef fixturedef;
		b2PolygonShape mShape;
		mShape.SetAsBox(float(w) / 2.0f / PTM_RATIO, float(h) / 2.0f / PTM_RATIO);
		fixturedef.shape = &mShape;
		fixturedef.density = 1.0f;
		fixturedef.friction = 0.3f;
		fixturedef.filter.categoryBits = C_OBSTACLE;
		fixturedef.filter.maskBits = C_AIRPLANE;
		wall1->CreateFixture(&fixturedef);
	}
	else if (a == 2)
	{
		CCSprite* mBox = CCSprite::create("HelloWorld.png", CCRectMake(0, 0, w, h));
		mBox->setPosition(ccp(x, y));
		mBox->setTag(1);
		mLayer->addChild(mBox, 1);

		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		bodyDef.position.Set(((float)x + (float)w / 2.0f) / PTM_RATIO, ((float)y + (float)h / 2) / PTM_RATIO);
		bodyDef.userData = mBox;
		wall2 = mWorld->CreateBody(&bodyDef);

		b2FixtureDef fixturedef;
		b2PolygonShape mShape;
		mShape.SetAsBox(float(w) / 2.0f / PTM_RATIO, float(h) / 2.0f / PTM_RATIO);
		fixturedef.shape = &mShape;
		fixturedef.density = 1.0f;
		fixturedef.friction = 0.3f;
		fixturedef.filter.categoryBits = C_OBSTACLE;
		fixturedef.filter.maskBits = C_AIRPLANE;
		wall2->CreateFixture(&fixturedef);
	}
}

void mLevel::createRectengularFixture(CCTMXLayer* layer, int x, int y, float width, float height)
{
	CCPoint p = layer->positionAt(ccp(x, y));
	CCSize tileSize = this->map->getTileSize();

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set((p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y - 25 + (tileSize.height / 2.0f)) / PTM_RATIO);
	b2Body* body = mWorld->CreateBody(&bodyDef);

	// define the shape
	b2PolygonShape shape;
	shape.SetAsBox((tileSize.width / PTM_RATIO) * 0.5f * width,
		(tileSize.height / PTM_RATIO) * 1.5f*height);

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 100.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.filter.categoryBits = C_OBSTACLE;
	fixtureDef.filter.maskBits = C_AIRPLANE;
	body->CreateFixture(&fixtureDef);
}

void mLevel::createFixtures(CCTMXLayer* layer)
{

	CCSize layerSize = layer->getLayerSize();

	for (int y = 0; y < layerSize.height; y++)
	{
		for (int x = 0; x < layerSize.width; x++)
		{
			// create a fixture if this tile has a sprite
			CCSprite* tileSprite = layer->tileAt(ccp(x, y));
			if (tileSprite)
				this->createRectengularFixture(layer, x, y, 1.0f, 1.0f);
		}
	}

}

CCTMXTiledMap* mLevel::createTimeTrialStage(const char* filename, b2World* world, CCLayer* layer)
{



	mWorld = world;
	mLayer = layer;
	map = CCTMXTiledMap::create(filename);
	background = map->layerNamed("Tile Layer 1");
	if (background != NULL)
		this->createFixtures(background);


	CCTMXObjectGroup* group = map->objectGroupNamed("Object Layer 1");
	CCArray* objects = group->getObjects();

	CCDictionary* dict = NULL;
	CCObject* pObj = NULL;
	int x, y, dir, type;
	
	CCARRAY_FOREACH(objects, pObj)
	{
		dict = (CCDictionary*)pObj;

		if (!dict)
			break;


		const char*	key = "y";
		y = ((CCString*)dict->objectForKey(key))->intValue();
		key = "type";
		type = ((CCString*)dict->objectForKey(key))->intValue();

		if (type == 1)   // Finish Coordinate	
		{
			finishcoordinate = y;
		}

	}
	return map;
}

