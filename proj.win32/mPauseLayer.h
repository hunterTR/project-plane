#pragma once

#include "cocos2d.h"
#include "HelloWorldScene.h"
#include "mMenuScene.h"
USING_NS_CC;
class mPauseLayer : public cocos2d::CCLayer
{
public:


	mPauseLayer(void);
	~mPauseLayer(void);

	virtual bool init();

	void menuResume(CCObject* pSender);
	void menuRestart(CCObject* pSender);
	void menuQuit(CCObject* pSender);
	void menuMain(CCObject* pSender);
	void getpausetime();
	cc_timeval starttime, endtime;
	CREATE_FUNC(mPauseLayer);
private: 
	int gameType;
};

