#pragma once
#include "cocos2d.h"
#include "mMenuScene.h"
class mCreditsLayer : public CCLayer
{
public:
	static cocos2d::CCScene* scene();

	virtual bool init();

	void menuCloseCallback(CCObject* pSender);

	mCreditsLayer(void);
	~mCreditsLayer(void);

	CREATE_FUNC(mCreditsLayer);
};

