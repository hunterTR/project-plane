#include "mAirPlane.h"


mAirPlane::mAirPlane(int x1,int y1)
{
	x = x1;
	y = y1;
	pSprite = CCSprite::create();
	width = 50;
	height = 50;
	directionway = 0;
	speed = 0;
	increase = 0;
	planespeed = 3.0f;
	frames = CCArray::create();
	frames->initWithCapacity(5);
	
	cacher = CCSpriteFrameCache::sharedSpriteFrameCache();
	cacher->addSpriteFramesWithFile("plane.plist");
	for (int i = 1; i<6; i++)
	{
		CCString *filename = CCString::createWithFormat("%d.png", i);
		frames->addObject(cacher->spriteFrameByName(filename->getCString()));

	}
	pSprite->initWithSpriteFrameName("1.png");

	pSprite->setTag(2);

}


mAirPlane::~mAirPlane()
{
}



void mAirPlane::setWorldandcreatebody(b2World* w)
{
	world = w;

	b2BodyDef bodydef;
	bodydef.type = b2_dynamicBody;
	bodydef.position.Set((float)x/ PTM_RATIO, (float)y/ PTM_RATIO);
	bodydef.userData = pSprite;
	pBody = world->CreateBody(&bodydef);

	b2PolygonShape shape;

/*	b2Vec2 vertices[3];
	vertices[0].Set(-1.f, 0.0f);
	vertices[1].Set(1.f, 0.0f);
	vertices[2].Set(0.0f, 2.0f);

	shape.Set(vertices, 3);
	*/
	shape.SetAsBox(25 / 2.0f / PTM_RATIO, 25 / 2.0f / PTM_RATIO);
	b2FixtureDef mfixture;
	mfixture.shape = &shape;
	mfixture.density = 1.0f;
	mfixture.friction = 0.3f;
	mfixture.filter.categoryBits = C_AIRPLANE;
	mfixture.filter.maskBits = C_OBSTACLE;

	pBody->CreateFixture(&mfixture);
	

}


void mAirPlane::move()
{
	// We are going to set the plane direction 
	if (directionway == 0)
	{//WE have to change sprite according to direction way. (LATER)
		pBody->SetLinearVelocity(b2Vec2(-planespeed - 0.3f-increase, -planespeed +1.0f - increase));
		
		speed = 0.5f + increase;
		pSprite->initWithSpriteFrameName("5.png");
	}
	else if (directionway == 1)
	{
		pBody->SetLinearVelocity(b2Vec2(-planespeed - increase, -planespeed -1.2f - increase));
		speed = 1.0f + increase;
		pSprite->initWithSpriteFrameName("4.png");
	}
	else if (directionway == 2)
	{
		pBody->SetLinearVelocity(b2Vec2(-0.0f , -planespeed-2.5f -increase));
		speed = 1.5f + increase;
		pSprite->initWithSpriteFrameName("3.png");
	}
	else if (directionway == 3)
	{
		pBody->SetLinearVelocity(b2Vec2(planespeed + increase,- planespeed-1.2f - increase));
		speed = 1.0f + increase;
		pSprite->initWithSpriteFrameName("2.png");
	}
	else if (directionway == 4)
	{
		pBody->SetLinearVelocity(b2Vec2(planespeed + 0.3f + increase,-planespeed +1.0f - increase));
		speed = 0.5f + increase;
		pSprite->initWithSpriteFrameName("1.png");
	}


}

void mAirPlane::changedirection(int clickway)
{
	if (clickway == 0) // if left side of screen has been clicked.
	{  
		if (directionway >= 0)  // there is only 5 way that plane can go. So it can't be below 0
			directionway--;
	}

	else if (clickway == 1) // if right side of screen has been clicked.
	{
		if (directionway <= 4)  // same thing ,  it can only be maximum 4
		directionway++;
	
	}

}

void mAirPlane::increaseSpeed()
{
	increase += 0.01;


}