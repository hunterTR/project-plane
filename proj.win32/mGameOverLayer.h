#pragma once
#include "cocos2d.h"
#include "mMenuScene.h"
#include "HelloWorldScene.h"

USING_NS_CC;
class mGameOverLayer : public cocos2d::CCLayer
{
public:
	mGameOverLayer(void);
	~mGameOverLayer(void);



	virtual bool init();

	void menuMain(CCObject* pSender);
	void menuRestart(CCObject* pSender);
	void menuQuit(CCObject* pSender);

	CREATE_FUNC(mGameOverLayer);

private:
	int gameType;
	int gameOverType;
	CCLabelTTF* bestScore;
};

