#pragma once

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "mMenuScene.h"

class mSplashScreen : public cocos2d::CCLayer
{
public:

	static cocos2d::CCScene* createscene();
	virtual bool init();

	void toMenuScene(float dt);

	CREATE_FUNC(mSplashScreen);
};
