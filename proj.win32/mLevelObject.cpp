#include "mLevelObject.h"



mObject::mObject(void)
{
	width = 50.0f;
	height = 50.0f;
}


mObject::~mObject(void)
{
	//pBody->GetWorld()->DestroyBody( pBody );
}


void mObject::setWorldandcreatebody(b2World* w)
{
	world = w;

	b2BodyDef bodydef;
	bodydef.type = b2_staticBody;
	bodydef.position.Set((float)-1000 / PTM_RATIO, (float)-1000 / PTM_RATIO);
	bodydef.userData = pSprite;
	pBody = world->CreateBody(&bodydef);

	b2PolygonShape shape;
	shape.SetAsBox((width / PTM_RATIO) / 2, (height / PTM_RATIO) / 2);

	b2FixtureDef mfixture;
	mfixture.shape = &shape;
	mfixture.isSensor = true;
	mfixture.density = 1.0f;
	mfixture.friction = 0.3f;
	pBody->CreateFixture(&mfixture);



}

CCSprite* mObject::getSprite()
{
	return pSprite;

}

b2Body* mObject::getBody()
{
	return pBody;

}

void mObject::setPositionandRotation(CCPoint pos, float angle)
{
	pSprite->setRotation(angle);
	pSprite->setPosition(pos);
}

void mObject::setWidthandHeight(float w, float h)
{

	width = w;
	height = h;


}
void mObject::changeFilter(uint16 category)
{
	b2Fixture* f = pBody->GetFixtureList();
	b2Filter mFilter = f->GetFilterData();
	mFilter.categoryBits = category;
	f->SetFilterData(mFilter);

}

b2Vec2 mObject::getPositioninb2Vec2()
{

	return pBody->GetPosition();


}

void mObject::setPosition(CCPoint pos)
{
	b2Vec2 vec;
	vec.x = pos.x / PTM_RATIO;
	vec.y = pos.y / PTM_RATIO;

	pBody->SetTransform(vec, 0.f);

}

bool mObject::isTouching(CCPoint location)
{
	CCPoint distance = ccpSub(pSprite->getPosition(), location);
	float distance2 = ccpDistance(pSprite->getPosition(), location);

	if (distance2 < 20)
		return true;

	return false;
}