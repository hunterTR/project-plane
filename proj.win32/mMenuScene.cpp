#include "mMenuScene.h"

using namespace cocos2d;

mMenuScene::mMenuScene(void)
{
}


mMenuScene::~mMenuScene(void)
{
}


CCScene* mMenuScene::createscene()
{

	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();

		CC_BREAK_IF(!scene);

		// 'layer' is an autorelease object
		mMenuScene *layer = mMenuScene::create();
		CC_BREAK_IF(!layer);

		// add layer as a child to scene
		scene->addChild(layer);

	} while (0);

	// return the scene
	return scene;
}

bool mMenuScene::init()
{
	bool bRet = false;

	do
	{

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());

		this->setTag(4);
		this->setTouchEnabled(true);

		bool isSoundOn = CCUserDefault::sharedUserDefault()->getBoolForKey("sound", 1);
		if (isSoundOn)
		{
			CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("intro.wav");
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("intro.wav", true);
			CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(10);
		}
		/*
		//CCSprite *background = CCSprite::create("background.png");
		//this->addChild(background);
		background->setAnchorPoint(ccp(0, 0));
		background->setPosition(ccp(0, 0));
		*/
		
		this->setTag(1);
		sky = CCSprite::create("sky.png");
		sky->setAnchorPoint(ccp(0, 0));
		sky->setPosition(ccp(0, -768));
		this->addChild(sky);

		background = CCSprite::create("background.png");
		background->setAnchorPoint(ccp(0, 0));
		background->setPosition(ccp(0, -768));
		this->addChild(background);

		sides = CCSprite::create("sides.png");
		sides->setAnchorPoint(ccp(0, 0));
		sides->setPosition(ccp(0, -768));
		this->addChild(sides, 10);
	
		CCSprite* logo =CCSprite::create("logo.png");
		logo->setPosition(ccp(500, 680));

		this->addChild(logo);
		CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
			"off.png",
			"off2.png",
			this,
			menu_selector(mMenuScene::menuCloseCallback));
		CC_BREAK_IF(!pCloseItem);
		CCMenuItemImage *playItem = CCMenuItemImage::create(
			"play.png",
			"playpush.png",
			this,
			menu_selector(mMenuScene::menuPlay));
		CC_BREAK_IF(!playItem);
		CCMenuItemImage *instructionItem = CCMenuItemImage::create(
			"trial.png",
			"trial1.png",
			this,
			menu_selector(mMenuScene::menuTrial));
		CC_BREAK_IF(!playItem);
		CCMenuItemImage *optionsItem = CCMenuItemImage::create(
			"op.png",
			"op1.png",
			this,
			menu_selector(mMenuScene::menuOptions));
		CC_BREAK_IF(!optionsItem);
	
		CCMenuItemImage *creditsItem = CCMenuItemImage::create(
			"cre.png",
			"cre2.png",
			this,
			menu_selector(mMenuScene::menuCredits));
		CC_BREAK_IF(!creditsItem);

		// Place the menu item bottom-right conner.
		pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 140, 40));

		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();


		CCMenu* cmenu = CCMenu::create(pCloseItem, NULL);
		cmenu->setPosition(CCPointZero);
		CCMenu* pMenu = CCMenu::create(playItem, instructionItem, optionsItem, creditsItem, NULL);
		pMenu->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height / 2 -30));
		pMenu->alignItemsVerticallyWithPadding(20.0f);


		CC_BREAK_IF(!pMenu);
		CC_BREAK_IF(!cmenu);


		
		this->addChild(cmenu, 100);
		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(pMenu, 1);

		CCLabelTTF* comingsoon = CCLabelTTF::create("COMING SOON!", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		comingsoon->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height / 2 - 30));
		this->addChild(comingsoon,12);

		highscorelabel = CCLabelTTF::create(" ", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		highscorelabel->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width -100 , CCDirector::sharedDirector()->getWinSize().height - 50));
		highscorelabel->setColor(ccc3(255, 255, 255));
		this->addChild(highscorelabel, 11);
		
		int highscore = CCUserDefault::sharedUserDefault()->getIntegerForKey("highscore", 1);
		CCString* str = CCString::createWithFormat("High Score : %d", highscore);
		highscorelabel->setString(str->getCString());




		bRet = true;
	} while (0);

	return bRet;
}


void mMenuScene::menuCloseCallback(CCObject* pSender)
{
	// "close" menu item clicked
	CCDirector::sharedDirector()->end();
}

void mMenuScene::menuPlay(CCObject* pSender)
{
	CCScene * scene = NULL;
	do
	{

		scene = HelloWorld::scene();

		CC_BREAK_IF(!scene);
		
		/*	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("menuclick.wav");      //SOUND EFFECT
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menuclick.wav");*/

		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.5,scene));
	} while (0);
	this->removeFromParentAndCleanup(true);
}


void mMenuScene::menuOptions(CCObject* pSender)
{
	CCScene * scene = NULL;
	do
	{

		scene = mOptionsLayer::scene();
		CC_BREAK_IF(!scene);

		/*	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("menuclick.wav");      //SOUND EFFECT
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menuclick.wav");*/
		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);


}
void mMenuScene::menuTrial(CCObject* pSender)
{
	/*	CCScene * scene = NULL;
	do
	{

		scene = mTimeTrialScene::scene();
		CC_BREAK_IF(!scene);


		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.5, scene));
	} while (0);
	*/
}

void mMenuScene::menuCredits(CCObject* pSender)
{

	CCScene * scene = NULL;
	do
	{

		scene = mCreditsLayer::scene();
		CC_BREAK_IF(!scene);

		/*	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("menuclick.wav");      //SOUND EFFECT
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menuclick.wav");*/

		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.5, scene));
	} while (0);

}



void mMenuScene::updateScreen(float dt)
{


}