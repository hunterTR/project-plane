#pragma once
#include "mLevelObject.h"

class mAirPlane : public mObject
{
public:
	
	
	mAirPlane(int x1,int y1);
	~mAirPlane();

	void setWorldandcreatebody(b2World* w);
	void move();
	void changedirection(int clickway);
	void UpdateImage(char * PngName);
	void increaseSpeed();

	float speed;
	float planespeed;

private:
	int directionway;
	CCSpriteFrameCache* cacher;
	CCArray* frames;
	float increase;
};

