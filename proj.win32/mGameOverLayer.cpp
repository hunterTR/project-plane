#include "mGameOverLayer.h"


mGameOverLayer::mGameOverLayer(void)
{

}


mGameOverLayer::~mGameOverLayer(void)
{

}


bool mGameOverLayer::init()
{
	bool bRet = false;

	do
	{

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());


		this->setTouchEnabled(true);

		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		// 1. Add a menu item with "X" image, which is clicked to quit the program.

		// Create a "close" menu item with close icon, it's an auto release object.
		gameType = CCUserDefault::sharedUserDefault()->getIntegerForKey("gametype", 1);
		gameOverType = CCUserDefault::sharedUserDefault()->getIntegerForKey("gameovertype", 1);

		bestScore = CCLabelTTF::create(" ", "abc", 40, CCSizeMake(640, 40), kCCTextAlignmentCenter);
		bestScore->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 150));
		bestScore->setColor(ccc3(255, 255, 255));
		this->addChild(bestScore);

		CCMenuItemImage *pMain = CCMenuItemImage::create(
			"main.png",
			"main2.png",
			this,
			menu_selector(mGameOverLayer::menuMain));
		CC_BREAK_IF(!pMain);

		CCMenuItemImage *pRestart = CCMenuItemImage::create(
			"rest.png",
			"rest2.png",
			this,
			menu_selector(mGameOverLayer::menuRestart));
		CC_BREAK_IF(!pRestart);

		CCMenuItemImage *pQuit = CCMenuItemImage::create(
			"off.png",
			"off2.png",
			this,
			menu_selector(mGameOverLayer::menuQuit));
		CC_BREAK_IF(!pQuit);

		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		if (gameType == 1)
		{
			int best = CCUserDefault::sharedUserDefault()->getIntegerForKey("actualscore", 1);
			CCString* temp = CCString::createWithFormat("Your Score: %d", best);
			bestScore->setString(temp->getCString());
		
		}
		else if (gameType==2 && gameOverType==1)
		{
		/*	int best = CCUserDefault::sharedUserDefault()->getIntegerForKey("besttime", 1); // WE DON't NEED TO PRINT HIGHEST SCORES ON GAME OVER SCREEN.
			CCString* temp = CCString::createWithFormat("%d", best);
			bestScore->setString(temp->getCString());

			int actualstage = CCUserDefault::sharedUserDefault()->getDoubleForKey("actualstage", 1);
			CCString* stagename = CCString::createWithFormat("stage%2d", stagenumber);
			CCUserDefault::sharedUserDefault()->getDoubleForKey(stagename->getCString());*/

		}

		// Create a menu with the "close" menu item, it's an auto release object.
		CCMenu* pMenu = CCMenu::create(pMain, pRestart, pQuit, NULL);
		pMenu->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height / 2));
		pMenu->alignItemsVerticallyWithPadding(20.0f);

		CC_BREAK_IF(!pMenu);

		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(pMenu, 1);
		bRet = true;
	} while (0);

	return bRet;
}



void mGameOverLayer::menuMain(CCObject* pSender)
{
	this->getParent()->getChildByTag(1)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(2)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(81)->removeFromParentAndCleanup(true);
	CCScene * scene = NULL;
	do
	{

		scene = mMenuScene::createscene();

		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);

	this->removeFromParentAndCleanup(true);

}

void mGameOverLayer::menuRestart(CCObject* pSender)
{
	this->getParent()->getChildByTag(1)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(2)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(81)->removeFromParentAndCleanup(true);
	//this->getParent()->getChildByTag(1)->removeFromParentAndCleanup(true);
	CCScene * scene = NULL;
	do
	{

		scene = HelloWorld::scene();

		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);

	this->removeFromParentAndCleanup(true);

}

void mGameOverLayer::menuQuit(CCObject* pSender)
{
	CCDirector::sharedDirector()->end();

}