#pragma once
#include "cocos2d.h"
#include "mAirPlane.h"
#include "mPauseLayer.h"
#include "mLayerColor.h"


class mIndependentLayer : public CCLayer
{
public:

	mIndependentLayer();
	~mIndependentLayer();

	virtual bool init();
	static cocos2d::CCScene* scene();
	void menuCloseCallback(CCObject* pSender);
	void pausegame();
	// implement the "static node()" method manually
	CREATE_FUNC(mIndependentLayer);

private:
	CCLabelTTF* highscore;
	int score;
	int highs;
	int gameType;

};

