#include "extendedparallax.h"

class CCPointObject : cocos2d::CCObject {
	CC_SYNTHESIZE(CCPoint, m_tRatio, Ratio)
	CC_SYNTHESIZE(CCPoint, m_tOffset, Offset)
	CC_SYNTHESIZE(CCNode *, m_pChild, Child)	// weak ref
};


extendedparallax::extendedparallax(void)
{
	//CCParallaxNode::CCParallaxNode() ;
}


extendedparallax::~extendedparallax(void)
{
}
extendedparallax * extendedparallax::node()
{
	return new extendedparallax();
}

void extendedparallax::increaseOffset(CCPoint offset, CCNode* node)
{
	for (unsigned int i = 0; i < m_pParallaxArray->num; i++) {
		CCPointObject *point = (CCPointObject *)m_pParallaxArray->arr[i];
		CCNode * curNode = point->getChild();
		if (curNode->isEqual(node)) {
			point->setOffset(ccpAdd(point->getOffset(), offset));
			break;
		}
	}
}