#pragma once

#include "cocos2d.h"

#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include "mCreditsLayer.h"
#include "mOptionsLayer.h"
#include "mTimeTrialScene.h"

class mMenuScene : public cocos2d::CCLayer
{
public:

	static cocos2d::CCScene* createscene();
	virtual bool init();
	mMenuScene(void);
	~mMenuScene(void);

	void menuPlay(CCObject* pSender);
	void menuTrial(CCObject* pSender);
	void menuOptions(CCObject* pSender);
	void menuCredits(CCObject* pSender);
	void menuCloseCallback(CCObject* pSender);

	void updateScreen(float dt);
	CCLabelTTF* ttf1;
	CCLabelTTF* highscorelabel;
	CREATE_FUNC(mMenuScene);

private:
	CCSprite* background;
	CCSprite* sides;
	CCSprite* sky;
};

