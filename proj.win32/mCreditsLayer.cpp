#include "mCreditsLayer.h"


mCreditsLayer::mCreditsLayer(void)
{
}


mCreditsLayer::~mCreditsLayer(void)
{
}

CCScene* mCreditsLayer::scene()
{

	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();

		CC_BREAK_IF(!scene);

		// 'layer' is an autorelease object
		mCreditsLayer *layer = mCreditsLayer::create();
		CC_BREAK_IF(!layer);

		// add layer as a child to scene
		scene->addChild(layer);

	} while (0);

	// return the scene
	return scene;
}


bool mCreditsLayer::init()
{
	bool bRet = false;

	do
	{

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());


		this->setTouchEnabled(true);

		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		// 1. Add a menu item with "X" image, which is clicked to quit the program.

		// Create a "close" menu item with close icon, it's an auto release object.
		CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
			"back.png",
			"back2.png",
			this,
			menu_selector(mCreditsLayer::menuCloseCallback));
		CC_BREAK_IF(!pCloseItem);


		// Place the menu item bottom-right conner.
		pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 100, 40));

		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		// Create a menu with the "close" menu item, it's an auto release object.
		CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
		pMenu->setPosition(CCPointZero);
		CC_BREAK_IF(!pMenu);

		CCSprite* cem = CCSprite::create("ahmetcem.png");
		CCSprite* sercan = CCSprite::create("sercan.png");
		cem->setPosition(ccp(510, 300));
		sercan->setPosition(ccp(510, 240));
		this->addChild(cem);
		this->addChild(sercan);
		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(pMenu, 1);
		CCLabelTTF* counter;

		counter = CCLabelTTF::create("Programming : Ahmet Cem KAYA", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		counter->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 50));
		counter->setColor(ccc3(255, 255, 255));
		this->addChild(counter, 11);

		CCLabelTTF* counter2;

		counter2 = CCLabelTTF::create("Graphics and Design : Sercan Turkmen", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		counter2->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 100));
		counter2->setColor(ccc3(255, 255, 255));
		this->addChild(counter2, 11);


		CCLabelTTF* counter3;

		counter3 = CCLabelTTF::create("Music and Sounds : Alexander Scharf", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		counter3->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 150));
		counter3->setColor(ccc3(255, 255, 255));
		this->addChild(counter3, 11);


		CCLabelTTF* counter4;

		counter4 = CCLabelTTF::create("SELI GAMES", "abc.tft", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		counter4->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 400));
		counter4->setColor(ccc3(255, 255, 255));
		this->addChild(counter4, 11);


		bRet = true;
	} while (0);

	return bRet;


}

void mCreditsLayer::menuCloseCallback(CCObject* pSender)
{
	CCScene * scene = NULL;
	do
	{

		scene = mMenuScene::createscene();
		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);

}