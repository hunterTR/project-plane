#pragma once

#include "cocos2d.h"
#include "mMenuScene.h"
USING_NS_CC;
class mOptionsLayer : public CCLayer
{
public:

	static cocos2d::CCScene* scene();

	virtual bool init();

	void menuCloseCallback(CCObject* pSender);
	void soundOnOff(CCObject* pSender);

	CCLabelTTF* ttf1;
	CCLabelTTF* soundttf1;
	mOptionsLayer(void);
	~mOptionsLayer(void);



	CREATE_FUNC(mOptionsLayer);
};

