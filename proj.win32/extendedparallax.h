#pragma once


#include "cocos2d.h"

USING_NS_CC;

class extendedparallax : public cocos2d::CCParallaxNode
{
public:
	extendedparallax(void);
	~extendedparallax(void);

	static extendedparallax* node();
	void increaseOffset(CCPoint offset, CCNode* node);


};

