#include "mPauseLayer.h"


mPauseLayer::mPauseLayer(void)
{
}


mPauseLayer::~mPauseLayer(void)
{
}

bool mPauseLayer::init()
{
	bool bRet = false;

	do
	{


		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());


		CCTime::gettimeofdayCocos2d(&starttime, NULL);
		this->setTouchEnabled(true);


		gameType = CCUserDefault::sharedUserDefault()->getIntegerForKey("gametype", 1);

		CCMenuItemImage *pResume = CCMenuItemImage::create(
			"RG.png",
			"RG2.png",
			this,
			menu_selector(mPauseLayer::menuResume));
		CCMenuItemImage *pRestart = CCMenuItemImage::create(
			"rest.png",
			"rest2.png",
			this,
			menu_selector(mPauseLayer::menuRestart));
		CCMenuItemImage *pQuit = CCMenuItemImage::create(
			"main.png",
			"main2.png",
			this,
			menu_selector(mPauseLayer::menuMain));

		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		CCMenu* pMenu = CCMenu::create(pResume, pRestart, pQuit, NULL);
		pMenu->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height / 2));
		pMenu->alignItemsVerticallyWithPadding(20.0f);


		CC_BREAK_IF(!pMenu);
		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(pMenu, 1);
		bRet = true;
	} while (0);

	return bRet;
}



void mPauseLayer::menuResume(CCObject* pSender)
{
	getpausetime();
	this->getParent()->getChildByTag(1)->onEnter();
	this->getParent()->getChildByTag(2)->onEnter();
	this->getParent()->getChildByTag(81)->removeFromParentAndCleanup(true);
	this->removeFromParentAndCleanup(true);


}

void mPauseLayer::menuRestart(CCObject* pSender)
{
	CCScene * scene = NULL;
	this->getParent()->getChildByTag(1)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(2)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(81)->removeFromParentAndCleanup(true);
	if (gameType == 1)
	{
		do
		{

			scene = HelloWorld::scene();

			CC_BREAK_IF(!scene);

			CCDirector::sharedDirector()->replaceScene(scene);

		} while (0);

	}
	else if (gameType == 2)
	{
		do
		{

			scene = mTimeTrialScene::scene();

			CC_BREAK_IF(!scene);

			CCDirector::sharedDirector()->replaceScene(scene);

		} while (0);
	}
	this->removeFromParentAndCleanup(true);
}

void mPauseLayer::menuQuit(CCObject* pSender)
{
	CCDirector::sharedDirector()->end();

}

void mPauseLayer::menuMain(CCObject* pSender)
{
	CCScene * scene = NULL;
	this->getParent()->getChildByTag(1)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(2)->removeFromParentAndCleanup(true);
	this->getParent()->getChildByTag(81)->removeFromParentAndCleanup(true);
	do
	{

		scene = mMenuScene::createscene();

		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);

	this->removeFromParentAndCleanup(true);
}


void mPauseLayer::getpausetime()
{
	CCTime::gettimeofdayCocos2d(&endtime, NULL);
	int duration = CCTime::timersubCocos2d(&starttime, &endtime);
	int durationbefore = CCUserDefault::sharedUserDefault()->getDoubleForKey("pausetime", 0);
	duration = duration / 1000 + durationbefore;
	CCUserDefault::sharedUserDefault()->setDoubleForKey("pausetime", duration);
}