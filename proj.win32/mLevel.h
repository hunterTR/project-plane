#pragma once

#include "cocos2d.h"
#include "mAirPlane.h"
#include <time.h>
#include <stdlib.h>



class mLevel
{
public:
	mLevel(b2World* world,CCLayer* layer,CCSize* winsiz);
	~mLevel();

	void createRandomLevel(float round);
	void addGround(int x, int y, int w, int h);
	void addLittlePassage(int place,  int y, int h);
	void addSmallObstacle(int place, int y, int h);
	void addBigObstacle(int place,int y, int h);
	void addLongPassage(int y);
	void createwalls(int x, int y, int w, int h,int a);
	CCTMXTiledMap* createTimeTrialStage(const char* filename, b2World* world, CCLayer* layer);
	void createRectengularFixture(CCTMXLayer* layer, int x, int y, float width, float height); // It's almost same with addGround.
	void createFixtures(CCTMXLayer* layer);
	b2Body* wall1;
	b2Body* wall2;
private:
	b2World* mWorld;
	CCLayer* mLayer;
	CCSize* winsize;
	CCTMXTiledMap* map;
	CCTMXLayer* background;
	int randomlengthofobstacle;
	int randomforlittlepassage;
	int randomtypeofobstacle;
	int leftsiders;
	int rightsiders;
	int lastposition;
	int passagelong;
	int finishcoordinate;


};

