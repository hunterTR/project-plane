#include "mTimeTrialScene.h"
#include "AppMacros.h"

USING_NS_CC;



mTimeTrialScene::mTimeTrialScene()
{
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	mWorld = new b2World(gravity);
	mWorld->SetAllowSleeping(true);
	mWorld->SetContinuousPhysics(true);

	mAP = new mAirPlane(500, 600);
	levelcreator = new mLevel(mWorld, this, &winsize);

}


mTimeTrialScene ::~mTimeTrialScene()
{

	delete mWorld;
	mWorld = NULL;
}

CCScene* mTimeTrialScene::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();

	// 'layer' is an autorelease object
	mTimeTrialScene  *layer = mTimeTrialScene::create();
	mIndependentLayer* deneme = mIndependentLayer::create();

	// add layer as a child to scene
	scene->addChild(layer, 4);
	scene->addChild(deneme, 8);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool mTimeTrialScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	CCUserDefault::sharedUserDefault()->setIntegerForKey("gametype", 2);    // time trial or normal
	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	winsize = CCDirector::sharedDirector()->getWinSize();
	this->setTag(1);

	background = CCSprite::create("background.png");
	background->setAnchorPoint(ccp(0, 0));
	background->setPosition(ccp(0, -768));
	this->addChild(background);
		

	mAP->setWorldandcreatebody(mWorld);
	this->addChild(mAP->getSprite());


	timer = CCLabelTTF::create(" ", "Helvetica", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
	timer->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 50));
	timer->setColor(ccc3(255, 255, 255));
	this->addChild(timer);

	CCUserDefault::sharedUserDefault()->setDoubleForKey("pausetime", 0);
	CCTime::gettimeofdayCocos2d(&starttime, NULL);

	stagenumber = CCUserDefault::sharedUserDefault()->getIntegerForKey("stage",1);
	CCString* level = CCString::createWithFormat("stage%2d.tmx", stagenumber);
	//this->mMap = levelcreator->createTimeTrialStage(level->getCString(), mWorld, this);

	this->mMap = levelcreator->createTimeTrialStage("deneme.tmx", mWorld, this);
	mMap->setAnchorPoint(ccp(0, 0));
	mMap->setPosition(ccp(0, 0));                                                  //TMX
	this->addChild(mMap,2);

	this->schedule(schedule_selector(mTimeTrialScene::updateScreen));
	this->schedule(schedule_selector(mTimeTrialScene::printTime));
	return true;
}




void mTimeTrialScene::updateScreen(float dt)
{
	//printTime();

	int velocityIterations = 8;
	int positionIterations = 1;


	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	mWorld->Step(dt, velocityIterations, positionIterations);

	//CCLOG("entering iterating bodies");
	for (b2Body* b = mWorld->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL)
		{
			CCSprite* myActor = (CCSprite*)b->GetUserData();




			if (myActor->getTag() == 100)
			{
				//GAMEOVER BITCH!
				highs = CCUserDefault::sharedUserDefault()->getDoubleForKey("besttime", 0);
				int actualstage = CCUserDefault::sharedUserDefault()->getDoubleForKey("actualstage",1);
				CCString* stagename = CCString::createWithFormat("stage%2d", stagenumber);
				CCUserDefault::sharedUserDefault()->setDoubleForKey(stagename->getCString(), duration);  // we will gona use this for printing time on gameoverlayer.
				if (duration > highs)
				{
				
					CCUserDefault::sharedUserDefault()->setDoubleForKey("besttime", duration);
				}
				this->schedule(schedule_selector(HelloWorld::gameOverBro), 1.0f);
			}

			myActor->setPosition(CCPointMake(b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO));
			myActor->setRotation(-1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
		}
	}

	mAP->move();
	cameraMovement();

}

void mTimeTrialScene::menuCloseCallback(CCObject* pSender)
{


}



void mTimeTrialScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
	CCTouch* mTouch = (CCTouch*)(touches->anyObject());

	CCPoint location = mTouch->getLocation();
	location = CCDirector::sharedDirector()->convertToGL(location);

	if (location.x > winsize.width / 2)
	{
		mAP->changedirection(1);
	}
	else
	{
		mAP->changedirection(0);
	}




}

void mTimeTrialScene::cameraMovement()
{
	//CAMERA MOVEMENT ALGORITHM
	float x, y, z;
	this->getCamera()->getEyeXYZ(&x, &y, &z);


	y = mAP->pSprite->getPosition().y - 600;
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	this->getCamera()->setCenterXYZ(x, y, 0);
	this->getCamera()->setEyeXYZ(x, y, z);

	if (background->getPosition().y > y + 600)   // Background looping algorithm
		background->setPosition(ccp(x, y - 1536));

	timer->setPosition(ccp(x + 400, y + 600));

}

void mTimeTrialScene::pausegame()
{
	do{

		ccColor3B c = { 100, 100, 100 };
		mPauseLayer *pauselayer = mPauseLayer::create();
		mLayerColor *denemeamk = mLayerColor::create();
		denemeamk->setTag(81);
		denemeamk->setColor(c);
		denemeamk->setOpacity(100);
		CC_BREAK_IF(!pauselayer);
		this->getParent()->addChild(denemeamk, 5);
		this->getParent()->addChild(pauselayer, 8);


	} while (0);
	CCTime::gettimeofdayCocos2d(&starttime, NULL);
	this->onExit();


}


void mTimeTrialScene::printTime(float dt)
{
	CCTime::gettimeofdayCocos2d(&endtime, NULL);
	double pausetime = CCUserDefault::sharedUserDefault()->getDoubleForKey("pausetime", 0);
	duration = CCTime::timersubCocos2d(&starttime, &endtime);
	duration = duration / 1000 - pausetime;
	CCString* temp = CCString::createWithFormat("Time : %.2f", duration);
	timer->setString(temp->getCString());


}

void mTimeTrialScene::gameOverBro(float dt)
{
	do{
		ccColor4B c = { 100, 100, 0, 100 };
		mGameOverLayer *gameover = mGameOverLayer::create();
		CC_BREAK_IF(!gameover);
		mLayerColor *denemeamk = mLayerColor::create();
		denemeamk->setTag(81);
		denemeamk->setOpacity(100);
		this->getParent()->addChild(denemeamk, 5);
		this->getParent()->addChild(gameover, 8);

	} while (0);
	this->getParent()->getChildByTag(2)->onExit();
	this->onExit();
}