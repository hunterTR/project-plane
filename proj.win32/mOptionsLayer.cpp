#include "mOptionsLayer.h"


mOptionsLayer::mOptionsLayer(void)
{
}


mOptionsLayer::~mOptionsLayer(void)
{
}

CCScene* mOptionsLayer::scene()
{

	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();

		CC_BREAK_IF(!scene);

		// 'layer' is an autorelease object
		mOptionsLayer *layer = mOptionsLayer::create();
		CC_BREAK_IF(!layer);

		// add layer as a child to scene
		scene->addChild(layer);

	} while (0);

	// return the scene
	return scene;
}


bool mOptionsLayer::init()
{
	bool bRet = false;

	do
	{

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(!CCLayer::init());


		this->setTouchEnabled(true);

		bool isSoundOn = CCUserDefault::sharedUserDefault()->getBoolForKey("sound", 1);
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		// 1. Add a menu item with "X" image, which is clicked to quit the program.

		// Create a "close" menu item with close icon, it's an auto release object.
		CCMenuItemImage *SoundOn;

		if (!isSoundOn)
		{
			SoundOn = CCMenuItemImage::create(
				"levelbuttonlocked.png",
				"levelbuttonlocked.png",
				this,
				menu_selector(mOptionsLayer::soundOnOff));
			CC_BREAK_IF(!SoundOn);
		}
		else
		{
			SoundOn = CCMenuItemImage::create(
				"levelbutton.png",
				"levelbuttonpushed.png",
				this,
				menu_selector(mOptionsLayer::soundOnOff));
			CC_BREAK_IF(!SoundOn);


		}

		SoundOn->setPosition(ccp(110, 460));
		// Create a menu with the "close" menu item, it's an auto release object.
		CCMenu* sMenu = CCMenu::create(SoundOn, NULL);
		sMenu->setPosition(CCPointZero);
		CC_BREAK_IF(!sMenu);

		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(sMenu, 1);


		CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
			"back.png",
			"back2.png",
			this,
			menu_selector(mOptionsLayer::menuCloseCallback));
		CC_BREAK_IF(!pCloseItem);


		// Place the menu item bottom-right conner.
		pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 100, 40));



		// Create a menu with the "close" menu item, it's an auto release object.
		CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
		pMenu->setPosition(CCPointZero);
		CC_BREAK_IF(!pMenu);

		// Add the menu to HelloWorld layer as a child layer.
		this->addChild(pMenu, 1);



		ttf1 = CCLabelTTF::create(" ", "Helvetica", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		ttf1->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width / 2, CCDirector::sharedDirector()->getWinSize().height - 200));
		ttf1->setColor(ccc3(255, 0, 0));
		this->addChild(ttf1);

		soundttf1 = CCLabelTTF::create("SOUND", "Helvetica", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
		soundttf1->setPosition(ccp(110, 500));
		soundttf1->setColor(ccc3(255, 255, 255));
		this->addChild(soundttf1);


		bRet = true;
	} while (0);

	return bRet;


}

void mOptionsLayer::menuCloseCallback(CCObject* pSender)
{
	CCScene * scene = NULL;
	do
	{

		scene = mMenuScene::createscene();
		CC_BREAK_IF(!scene);

		CCDirector::sharedDirector()->replaceScene(scene);
	} while (0);
	this->removeFromParentAndCleanup(true);
}

void mOptionsLayer::soundOnOff(CCObject* pSender)
{
	bool isSoundOn = CCUserDefault::sharedUserDefault()->getBoolForKey("sound", 1);

	if (isSoundOn)
	{
		CCUserDefault::sharedUserDefault()->setBoolForKey("sound", false);
		ttf1->setString("sound: OFF");
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	}
	else
	{
		CCUserDefault::sharedUserDefault()->setBoolForKey("sound", true);
		ttf1->setString("sound: ON");
	}

}