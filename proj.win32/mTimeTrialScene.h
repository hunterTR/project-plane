#pragma once

#include "cocos2d.h"
#include "Box2D\Box2D.h"
#include "mAirPlane.h"
#include "mIndependentLayer.h"
#include "mLevel.h"
#include "mPauseLayer.h"
#include "mLayerColor.h"
#include "mGameOverLayer.h"
//  objeleri silmede hala problem olabilir ona dikkat ! ama �uanl�k iyi g�r�n�yor
//  u�a��n hala  h�z ayarlamas�na ihtiyac� var.
//  men�ler-sceneler halledilecek.
// round olay� de�i�ildi her level 30 obstacle dan olu�uyor. ( not gereksiz yere fazladan body ler ��k�yor gibiydi ama �uanl�k �yle bi�ey yok gibi)

class mTimeTrialScene : public cocos2d::CCLayer
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	mTimeTrialScene();
	~mTimeTrialScene();
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::CCScene* scene();

	void updateScreen(float dt);
	void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
	void cameraMovement();
	void addGround(const char* filename, int x, int y, int w, int h);
	void pausegame();
	// a selector callback
	void menuCloseCallback(CCObject* pSender);
	void printTime(float dt);
	void gameOverBro(float dt);
	// implement the "static node()" method manually
	CREATE_FUNC(mTimeTrialScene);


private:
	b2World* mWorld;
	mAirPlane* mAP;
	CCSize winsize;
	CCLabelTTF* timer;
	CCLabelTTF* highscore;
	mLevel* levelcreator;
	CCSprite* background;
	double duration;
	int highs;
	CCTMXTiledMap* mMap;
	cc_timeval starttime, endtime, pausetime;
	int stagenumber;

};


