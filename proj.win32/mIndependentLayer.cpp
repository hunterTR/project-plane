#include "mIndependentLayer.h"


mIndependentLayer::mIndependentLayer()
{
	score = 0;

}


mIndependentLayer::~mIndependentLayer()
{
}



bool mIndependentLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if (!CCLayer::init())
	{
		return false;
	}

	this->setTouchEnabled(true);

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	//winsize = CCDirector::sharedDirector()->getWinSize();



	this->setTag(2);

	CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
		"pause.png",
		"pause2.png",
		this,
		menu_selector(mIndependentLayer::menuCloseCallback));

	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width / 2,
		origin.y + pCloseItem->getContentSize().height / 2));

	gameType = CCUserDefault::sharedUserDefault()->getIntegerForKey("gametype", 1);
	// create menu, it's an autorelease object
	CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
	pMenu->setPosition(CCPointZero);
	this->addChild(pMenu, 1);


	highscore = CCLabelTTF::create(" ", "Helvetica", 20, CCSizeMake(640, 32), kCCTextAlignmentCenter);
	highscore->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 200, CCDirector::sharedDirector()->getWinSize().height - 50));
	highscore->setColor(ccc3(255, 255, 255));
	this->addChild(highscore);

	if (gameType == 1)
	{
		highs = CCUserDefault::sharedUserDefault()->getIntegerForKey("highscore", 1);

		CCString* s = CCString::createWithFormat("High Score: %d", highs);
		highscore->setString(s->getCString());
	}
	else if (gameType == 2)
	{
		highs = CCUserDefault::sharedUserDefault()->getIntegerForKey("besttime", 1);

		CCString* s = CCString::createWithFormat("Best Time: %d", highs);
		highscore->setString(s->getCString());

	}



	return true;

}


void mIndependentLayer::menuCloseCallback(CCObject* pSender)
{
	pausegame();
}

void mIndependentLayer::pausegame()
{
	do{

		ccColor3B c = { 100, 100, 100 };
		mPauseLayer *pauselayer = mPauseLayer::create();
		mLayerColor *denemeamk = mLayerColor::create();
		denemeamk->setTag(81);
		denemeamk->setColor(c);
		denemeamk->setOpacity(100);
		CC_BREAK_IF(!pauselayer);
		this->getParent()->addChild(denemeamk, 5);
		this->getParent()->addChild(pauselayer, 8);


	} while (0);
	this->onExit();
	this->getParent()->getChildByTag(1)->onExit();


}
