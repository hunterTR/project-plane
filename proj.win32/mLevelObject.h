#pragma once
#include "cocos2d.h"
#include "Box2D\Box2D.h"

USING_NS_CC;

#define PTM_RATIO 32
#define FLOOR_HEIGHT 62.0f
#define PTM 60
#define C_OBSTACLE 0x0001
#define C_AIRPLANE 0x0002


enum direction
{
	mLeft,
	mRight
};
class mObject
{
public:
	mObject(void);
	~mObject(void);
	CCSprite* getSprite();
	b2Body* getBody();
	void setPositionandRotation(CCPoint pos, float angle);
	void setWorldandcreatebody(b2World* w);
	void setWidthandHeight(float w, float h);
	void changeFilter(uint16 category);
	void setPosition(CCPoint pos);
	bool isTouching(CCPoint location);


	b2Vec2 getPositioninb2Vec2();
	CCSprite *pSprite;
	b2Body* pBody;
	CCPoint position;
	b2World* world;
	float x, y;
	float width, height;
private:
};

